package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/peterbourgon/diskv"
	"gitlab.com/kfreiman/timeservice/pkg/now"
)

func main() {
	// create server
	s := &http.Server{
		Addr:    ":8081",
		Handler: newHandler(), // set handler with defined routes
	}

	log.Printf("http server started on %s\n", s.Addr)

	// serve the server
	log.Fatal(s.ListenAndServe())
}

// create Handler for http.Server and setup routes
func newHandler() http.Handler {
	store := diskv.New(diskv.Options{
		BasePath:     "/tmp/timeservice-diskv",
		CacheSizeMax: 1024 * 1024, // 1MB
	})

	nowUsecase := now.NewUsecase(store)

	// pass usecase to HTTP JSON handler
	nowHandler := now.NewHandler(nowUsecase)

	r := chi.NewRouter()
	r.Route("/time", func(r chi.Router) {
		r.Get("/now", nowHandler.NowEndpoint())
		r.Get("/string", nowHandler.StringEndpoint())
		r.Get("/add", nowHandler.AddEndpoint())
		r.Post("/set", nowHandler.SetCorrectionEndpoint())
		r.Post("/reset", nowHandler.ResetCorrectionEndpoint())
	})
	return r
}
