FROM golang:1.11 AS build
WORKDIR /go/src/gitlab.com/kfreiman/timeservice
COPY . /go/src/gitlab.com/kfreiman/timeservice
RUN go build -o bin/app .

FROM gcr.io/distroless/base
WORKDIR /root/
COPY --from=build /go/src/gitlab.com/kfreiman/timeservice/bin/app .
CMD ["./app"]
EXPOSE 8081
