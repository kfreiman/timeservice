module gitlab.com/kfreiman/timeservice

require (
	github.com/go-chi/chi v3.3.4-0.20181210200124-def7567e149e+incompatible
	github.com/google/btree v0.0.0-20180813153112-4030bb1f1f0c // indirect
	github.com/peterbourgon/diskv v2.0.2-0.20180312054125-0646ccaebea1+incompatible
)
