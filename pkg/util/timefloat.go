package util

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"
)

// TimeAsFloat formats time.Time to float64 format
func TimeAsFloat(t time.Time) string {
	datetime := t.Format("060102.150405")
	msecs := int(math.Round(float64(t.Nanosecond()) / 1000000))

	msecsStr := ""
	if msecs > 0 {
		msecsStr = strings.TrimRight(fmt.Sprintf("%03d", msecs), "0")
	}

	return datetime + msecsStr
}

// FloatAsDuration converts string in AsFloat64 format to time.Duration
func FloatAsDuration(value string) (time.Duration, error) {
	floatVal, err := strconv.ParseFloat(value, 64)
	if err != nil {
		return time.Duration(0), err
	}

	isNegative := floatVal < 0

	floatVal = math.Abs(floatVal)

	years := math.Floor(floatVal / 10000)
	months := math.Floor(math.Mod(floatVal, 10000) / 100)
	days := math.Floor(math.Mod(floatVal, 100))
	hours := math.Floor(math.Mod(floatVal*100, 100))
	mins := math.Floor(math.Mod(floatVal*10000, 100))
	secs := math.Floor(math.Mod(floatVal*1000000, 100))

	d := time.Duration(years)*365*24*time.Hour +
		time.Duration(months)*24*30*time.Hour +
		time.Duration(days)*24*time.Hour +
		time.Duration(hours)*time.Hour +
		time.Duration(mins)*time.Minute +
		time.Duration(secs)*time.Second

	if isNegative {
		d = -d
	}
	return d, nil
}

// FloatAsTime converts string in AsFloat64 format to time.Duration
func FloatAsTime(value string) (time.Time, error) {
	floatVal, err := strconv.ParseFloat(value, 64)
	if err != nil {
		return time.Time{}, err
	}

	if floatVal < 0 {
		return time.Time{}, errors.New("Date can't be negative")
	}

	year := int(math.Floor(floatVal / 10000))
	if year < 0 || 99 < year {
		return time.Time{}, errors.New("Year must be in range 0-99")
	}
	if year >= 69 { // Unix time starts Dec 31 1969 in some time zones
		year += 1900
	} else {
		year += 2000
	}

	month := int(math.Floor(math.Mod(floatVal, 10000) / 100))
	if month <= 0 || 12 < month {
		return time.Time{}, errors.New("Month must be in range 1-12")
	}

	day := int(math.Floor(math.Mod(floatVal, 100)))
	if day < 1 || day > daysIn(time.Month(month), year) {
		return time.Time{}, errors.New("Day out of range")
	}

	hour := int(math.Floor(math.Mod(floatVal*100, 100)))
	if hour < 0 || 24 <= hour {
		return time.Time{}, errors.New("Hour must be in range 0-23")
	}

	min := int(math.Floor(math.Mod(floatVal*10000, 100)))
	if min < 0 || 60 <= min {
		return time.Time{}, errors.New("Minutes must be in range 0-59")
	}

	sec := int(math.Floor(math.Mod(floatVal*1000000, 100)))
	if sec < 0 || 60 <= sec {
		return time.Time{}, errors.New("Seconds must be in range 0-59")
	}

	msec := int(math.Floor(math.Mod(floatVal*1000000000, 1000)))

	return time.Date(year, time.Month(month), day, hour, min, sec, msec*1000000, time.UTC), nil
}

// daysBefore[m] counts the number of days in a non-leap year
// before month m begins. There is an entry for m=12, counting
// the number of days before January of next year (365).
var daysBefore = [...]int32{
	0,
	31,
	31 + 28,
	31 + 28 + 31,
	31 + 28 + 31 + 30,
	31 + 28 + 31 + 30 + 31,
	31 + 28 + 31 + 30 + 31 + 30,
	31 + 28 + 31 + 30 + 31 + 30 + 31,
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30,
	31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31,
}

//
func daysIn(m time.Month, year int) int {
	if m == time.February && isLeap(year) {
		return 29
	}
	return int(daysBefore[m] - daysBefore[m-1])
}

func isLeap(year int) bool {
	return year%4 == 0 && (year%100 != 0 || year%400 == 0)
}
