package util

import (
	"encoding/json"
	"net/http"
)

// H is a shortcut for map[string]interface{}
type H map[string]interface{}

// JSON Renders obj as json with content type
func JSON(w http.ResponseWriter, code int, obj interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	json.NewEncoder(w).Encode(obj)
}
