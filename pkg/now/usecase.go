package now

import (
	"log"
	"strconv"
	"time"
)

var deltaKey = "delta"

// TimeUsecase - interface described extern usecase methods
type TimeUsecase interface {
	// Now returns current time with correction
	Now() time.Time

	// SetCorrection updates correction
	SetCorrection(correctTime time.Time) error

	// ResetCorrection removes delta correction
	ResetCorrection() error
}

// NewUsecase creates and configure Usecase instance
func NewUsecase(store StoreInterface) *Usecase {
	return &Usecase{store: store}
}

// StoreInterface describes store's methods
// It needs for delta persisting
type StoreInterface interface {
	ReadString(key string) string
	WriteString(key string, val string) error
	Erase(key string) error
}

// Usecase - instance of usecase implementation
type Usecase struct {
	store StoreInterface
}

// getDelta returns stored correction
func (u *Usecase) getDelta() time.Duration {
	s := u.store.ReadString(deltaKey)
	if s == "" {
		return time.Duration(0)
	}

	delta, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		log.Println("Error during delta read string: " + err.Error())
		return time.Duration(0)
	}
	return time.Duration(delta)
}

// Now returns time with correction
func (u *Usecase) Now() time.Time {
	t := time.Now().UTC()
	t = t.Add(u.getDelta())
	return t
}

// ResetCorrection removes delta correction
func (u *Usecase) ResetCorrection() error {
	return u.store.Erase(deltaKey)
}

// SetCorrection stores correction
func (u *Usecase) SetCorrection(correctTime time.Time) error {
	current := time.Now().UTC()
	diff := current.Sub(correctTime)
	deltaStr := strconv.FormatInt(-diff.Nanoseconds(), 10)
	return u.store.WriteString(deltaKey, deltaStr)
}
