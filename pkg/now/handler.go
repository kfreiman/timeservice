package now

import (
	"net/http"
	"time"

	"gitlab.com/kfreiman/timeservice/pkg/util"
)

// Handler - implementation of handler compatible with http.Handler
type Handler struct {
	usecase TimeUsecase
}

// NewHandler returns http.Handler implementation with defined endpoints
func NewHandler(u TimeUsecase) *Handler {
	h := &Handler{
		usecase: u,
	}
	return h
}

// NowEndpoint returns JSON result of usecase's Now method
func (h *Handler) NowEndpoint() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t := h.usecase.Now()
		util.JSON(w, 200, util.H{"time": util.TimeAsFloat(t)})
	}
}

// StringEndpoint returns JSON result of usecase's String method
func (h *Handler) StringEndpoint() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		value := r.URL.Query().Get("time")

		t, err := util.FloatAsTime(value)
		if err != nil {
			util.JSON(w, 400, util.H{"message": err.Error()})
			return
		}

		util.JSON(w, 200, util.H{"str": t.Format(time.RFC3339Nano)})
	}
}

// AddEndpoint returns JSON result of usecase's Add method
func (h *Handler) AddEndpoint() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		value := r.URL.Query().Get("time")
		t, err := util.FloatAsTime(value)
		if err != nil {
			util.JSON(w, 400, util.H{"message": err.Error()})
			return
		}

		delta := r.URL.Query().Get("delta")
		d, err := util.FloatAsDuration(delta)
		if err != nil {
			util.JSON(w, 400, util.H{"message": err.Error()})
			return
		}

		t = t.Add(d)
		util.JSON(w, 200, util.H{"time": util.TimeAsFloat(t)})
	}
}

// SetCorrectionEndpoint returns JSON result of usecase's SetCorrection method
func (h *Handler) SetCorrectionEndpoint() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		value := r.URL.Query().Get("time")
		t, err := util.FloatAsTime(value)
		if err != nil {
			util.JSON(w, 400, util.H{"message": err.Error()})
			return
		}

		err = h.usecase.SetCorrection(t)
		if err != nil {
			util.JSON(w, 500, util.H{"message": err.Error()})
			return
		}

		util.JSON(w, 200, nil)
	}
}

// ResetCorrectionEndpoint returns JSON result of usecase's ResetCorrection method
func (h *Handler) ResetCorrectionEndpoint() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := h.usecase.ResetCorrection()
		if err != nil {
			util.JSON(w, 500, util.H{"message": err.Error()})
			return
		}

		util.JSON(w, 200, nil)
	}
}
